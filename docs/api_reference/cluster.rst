.. _cluster:

Cluster
=======

The Cluster driver is separated into three layers:

- :ref:`api_reference/cluster:QCoDeS driver`: Instrument driver based on `QCoDeS <https://qcodes.github.io/Qcodes/>`_ and the instrument's native interface.

- :ref:`api_reference/cluster:Native interface`: Instrument API that provides control over the instrument and is an extension of the the SCPI interface.

- :ref:`api_reference/cluster:SCPI interface`: Instrument API based on the `SCPI <https://www.ivifoundation.org/docs/scpi-99.pdf>`_ standard which in turn is based on IEEE488.2.

.. warning::

	This is a preliminary driver for the Cluster that is currently under development.


QCoDeS driver
-------------

.. automodule:: cluster.cluster


QCoDeS parameters
^^^^^^^^^^^^^^^^^

`QCoDeS <https://qcodes.github.io/Qcodes/>`_ parameters generated by :class:`~cluster.cluster.cluster_qcodes`.

.. include:: ../_build/cluster_qcodes_param.rst

.. Note::

	Only module 1's parameters are listed, but all other modules have the same parameters.


Native interface
----------------

.. automodule:: cluster.cluster_ifc


SCPI interface
--------------

.. automodule:: cluster.cluster_scpi_ifc
