.. _spi_rack:

SPI Rack
==========

QCoDeS driver
-------------

.. automodule:: spi_rack.spi_rack
.. automodule:: spi_rack.spi_module_base


QCoDeS parameters
^^^^^^^^^^^^^^^^^

.. include:: ../_build/spi_rack_qcodes_param.rst

S4g module
-------------

.. automodule:: spi_rack.s4g_module

S4g QCoDeS parameters
^^^^^^^^^^^^^^^^^^^^^

.. include:: ../_build/s4g_qcodes_param.rst

D5a module
-------------
.. automodule:: spi_rack.d5a_module

D5a QCoDeS parameters
^^^^^^^^^^^^^^^^^^^^^

.. include:: ../_build/d5a_qcodes_param.rst
