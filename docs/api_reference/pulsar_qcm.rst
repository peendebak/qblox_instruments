.. _pulsar_qcm:

Pulsar QCM
==========

The Pulsar QCM driver is separated into three layers:

- :ref:`api_reference/pulsar_qcm:QCoDeS driver`: Instrument driver based on `QCoDeS <https://qcodes.github.io/Qcodes/>`_ and the instrument's native interface.

- :ref:`api_reference/pulsar_qcm:Native interface`: Instrument API that provides control over the instrument and is an extension of the the SCPI interface.

- :ref:`api_reference/pulsar_qcm:SCPI interface`: Instrument API based on the `SCPI <https://www.ivifoundation.org/docs/scpi-99.pdf>`_ standard which in turn is based on IEEE488.2.


QCoDeS driver
-------------

.. automodule:: pulsar_qcm.pulsar_qcm


QCoDeS parameters
^^^^^^^^^^^^^^^^^

`QCoDeS <https://qcodes.github.io/Qcodes/>`_ parameters generated by :class:`~pulsar_qcm.pulsar_qcm.pulsar_qcm_qcodes`.

.. Note::

	Only sequencer 0's parameters are listed, but all other sequencers have the same parameters.

.. include:: ../_build/pulsar_qcm_qcodes_param.rst


Native interface
----------------

.. automodule:: pulsar_qcm.pulsar_qcm_ifc


SCPI interface
--------------

.. automodule:: pulsar_qcm.pulsar_qcm_scpi_ifc
