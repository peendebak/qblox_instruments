.. _network_cfg:

Network adapter configuration
=============================


Windows
-------

1. Go to `Control Panel` > `Network and Internet` > `Network Connections`.
2. Select the network adapter that is connected to the same network as the module(s) (e.g. USB Ethernet adapter).
3. Right-click and select `properties`.
4. Select `Internet Protocol Version 4 (TCP/IPv4)` and click on properties.
5. Select `Use the following IP address` and specify the desired IP address (e.g. ``192.168.0.200``) and subnet mask (i.e. ``255.255.255.0``) 
6. Click on `OK` and `Close`.


Ubuntu
------

1. Go to `System Settings` > `Connections`.
2. Select the network adapter that is connected to the same network as the module(s) (e.g. USB Ethernet adapter).
3. Set `IPv4 method` to `Manual` and specify the desired IP address (e.g. ``192.168.0.200``) and subnet mask (i.e. ``255.255.255.0``).
4. Click on `Apply` and close the window.


MacOS
-----

1. Open `System Preferences` > `Network`.
2. Select the network adapter that is connected to the same network as the module(s) (e.g. USB Ethernet adapter).
3. Set `Configure IPv4` to `Manually` and specify the desired IP address (e.g. ``192.168.0.200``) and subnet mask (i.e. ``255.255.255.0``).
4. Click on `Apply` and close the window.