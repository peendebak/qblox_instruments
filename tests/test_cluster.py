#------------------------------------------------------------------------------
# Description    : Pulsar QCM test script
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
#------------------------------------------------------------------------------


#-- include -------------------------------------------------------------------

from tests           import generic
from cluster.cluster import cluster_dummy

#-- functions -----------------------------------------------------------------

#------------------------------------------------------------------------------
def test_get_scpi_commands():
    """
    Tests get SCPI commands function call. If no exceptions occur and the returned object matches
    the json schema the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        #Check SCPI commands
        generic.test_get_scpi_commands(clstr)
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()

#------------------------------------------------------------------------------
def test_get_idn():
    """
    Tests get IDN function call. If no exceptions occur and the returned object matches
    the json schema the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        #Check IDN
        generic.test_get_idn(clstr)
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()

#------------------------------------------------------------------------------
def test_scpi_commands():
    """
    Tests remaining mandatory SCPI commands. If no exceptions occur the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        #Check remaining mandatory commands
        generic.test_scpi_commands(clstr)
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()

#------------------------------------------------------------------------------
def test_get_system_status():
    """
    Tests get system status function call. If no exceptions occur the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        #Get system status (dummy returns nonsense status and flags)
        assert clstr.get_system_status() == {'status': '0', 'flags': []}
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()

#------------------------------------------------------------------------------
def test_get_temp():
    """
    Tests temperature readout function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        #Check temperature readout function calls
        generic.test_get_temp(clstr)
        assert clstr.get_current_bp_temperature_0() == 0.0
        assert clstr.get_maximum_bp_temperature_0() == 0.0
        assert clstr.get_current_bp_temperature_1() == 0.0
        assert clstr.get_maximum_bp_temperature_1() == 0.0
        assert clstr.get_current_bp_temperature_2() == 0.0
        assert clstr.get_maximum_bp_temperature_2() == 0.0
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()

#------------------------------------------------------------------------------
def test_ref_src():
    """
    Tests reference source setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        #Check reference source function calls
        generic.test_ref_src(clstr)
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()

#------------------------------------------------------------------------------
def test_module_present():
    """
    Tests module present function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Cluster
    clstr = cluster_dummy("cluster")

    try:
        for slot_idx in range(1, 21):
            assert clstr.get("module{}_present".format(slot_idx)) == "present"
    except Exception:
        raise
    finally:
        #Close Cluster
        clstr.close()