#------------------------------------------------------------------------------
# Description    : Pulsar QCM test script
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
#------------------------------------------------------------------------------


#-- include -------------------------------------------------------------------

from tests                 import generic
from pulsar_qcm.pulsar_qcm import pulsar_qcm_dummy

#-- functions -----------------------------------------------------------------

#------------------------------------------------------------------------------
def test_get_scpi_commands():
    """
    Tests get SCPI commands function call. If no exceptions occur and the returned object matches
    the json schema the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check SCPI commands
        generic.test_get_scpi_commands(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_get_idn():
    """
    Tests get IDN function call. If no exceptions occur and the returned object matches
    the json schema the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check IDN
        generic.test_get_idn(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_scpi_commands():
    """
    Tests remaining mandatory SCPI commands. If no exceptions occur the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check remaining mandatory commands
        generic.test_scpi_commands(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_get_system_status():
    """
    Tests get system status function call. If no exceptions occur the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Get system status (dummy returns nonsense status and flags)
        assert pulsar.get_system_status() == {'status': '0', 'flags': []}
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_get_temp():
    """
    Tests temperature readout function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check temperature readout function calls
        generic.test_get_temp(pulsar)
        assert pulsar.get_current_afe_temperature() == 0.0
        assert pulsar.get_maximum_afe_temperature() == 0.0
        assert pulsar.get_current_lo_temperature()  == 0.0
        assert pulsar.get_maximum_lo_temperature()  == 0.0
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_ref_src():
    """
    Tests reference source setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check reference source function calls
        generic.test_ref_src(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_out_amp_offset():
    """
    Tests output amplifier offset setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        generic.test_out_amp_offset(pulsar, 4)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_out_dac_offset():
    """
    Tests output DAC offset setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        generic.test_out_dac_offset(pulsar, 4, 2.5)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_waveform_handling(tmpdir):
    """
    Tests waveform handling (e.g. adding, deleting) function calls.

    Parameters
    ----------
    tmpdir
        Temporary directory

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check waveform handling function calls
        generic.test_waveform_weight_handling(pulsar, tmpdir)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_program_handling(tmpdir):
    """
    Tests program handling function calls.

    Parameters
    ----------
    tmpdir
        Temporary directory

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check program handling function calls
        generic.test_program_handling(pulsar, tmpdir)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_sequencer_control():
    """
    Tests program handling function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qcm_dummy("pulsar")

    try:
        #Check sequencer control function calls
        generic.test_sequencer_control(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()
