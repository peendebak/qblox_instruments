#------------------------------------------------------------------------------
# Description    : Pulsar QRM test script
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
#------------------------------------------------------------------------------


#-- include -------------------------------------------------------------------

import struct
import os
import json

from tests                 import generic
from pulsar_qrm.pulsar_qrm import pulsar_qrm_dummy

#-- functions -----------------------------------------------------------------

#------------------------------------------------------------------------------
def test_get_scpi_commands():
    """
    Tests get SCPI commands function call. If no exceptions occur and the returned object matches
    the json schema the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check SCPI commands
        generic.test_get_scpi_commands(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_get_idn():
    """
    Tests get IDN function call. If no exceptions occur and the returned object matches
    the json schema the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check IDN
        generic.test_get_idn(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_scpi_commands():
    """
    Tests remaining mandatory SCPI commands. If no exceptions occur the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check remaining mandatory commands
        generic.test_scpi_commands(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_get_system_status():
    """
    Tests get system status function call. If no exceptions occur the test passes.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Get system status (dummy returns nonsense status and flags)
        assert pulsar.get_system_status() == {'status': '0', 'flags': []}
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_get_temp():
    """
    Tests temperature readout function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check temperature readout function calls
        generic.test_get_temp(pulsar)
        assert pulsar.get_current_afe_temperature() == 0.0
        assert pulsar.get_maximum_afe_temperature() == 0.0
        assert pulsar.get_current_lo_temperature()  == 0.0
        assert pulsar.get_maximum_lo_temperature()  == 0.0
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_ref_src():
    """
    Tests reference source setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check reference source function calls
        generic.test_ref_src(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_in_amp_gain():
    """
    Tests input amplifier gain setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        for idx in range(0, 2):
            #Check input amplifier gain function calls
            pulsar.set("in{}_gain".format(idx), 0)

            #Check invalid gain settings
            try:
                pulsar.set("in{}_gain".format(idx), -7)
                assert False
            except Exception:
                pass

            try:
                pulsar.set("in{}_gain".format(idx), 26)
                assert False
            except Exception:
                pass

            #Dummy nonsense answer always returns 0dB
            assert pulsar.get("in{}_gain".format(idx)) == 0

    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_out_amp_offset():
    """
    Tests output amplifier offset setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        generic.test_out_amp_offset(pulsar, 2)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_out_dac_offset():
    """
    Tests output DAC offset setting and getting function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        generic.test_out_dac_offset(pulsar, 2, 1.0)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_waveform_weight_handling(tmpdir):
    """
    Tests waveform and weight handling (e.g. adding, deleting) function calls.

    Parameters
    ----------
    tmpdir
        Temporary directory

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check waveform and weight handling function calls
        generic.test_waveform_weight_handling(pulsar, tmpdir)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_acquisition_handling(tmpdir):
    """
    Tests waveform handling (e.g. adding, deleting) function calls.

    Parameters
    ----------
    tmpdir
        Temporary directory

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        acquisitions = {
                    "acq0": {"num_bins": 10, "index": 0},
                    "acq1": {"num_bins": 20, "index": 1},
                    "acq2": {"num_bins": 30, "index": 2},
                    "acq3": {"num_bins": 40, "index": 3}
                }

        #Add acquisitions (and program) to single dictionary and write to JSON file.
        wave_and_prog_dict = {"waveforms": {}, "weights": {}, "acquisitions": acquisitions, "program": "stop"}
        with open(os.path.join(tmpdir, "sequence.json"), 'w', encoding='utf-8') as file:
            json.dump(wave_and_prog_dict, file, indent=4)
            file.close()

        #Upload acquisitions
        pulsar.sequencer0_waveforms_and_program(os.path.join(tmpdir, "sequence.json"))

        #Store scope acquisitions
        for name in acquisitions:
            pulsar.store_scope_acquisition(0, name)

        #Get acquisitions
        acq_out = pulsar.get_acquisitions(0)

        #Check acquisition content
        sample_width     = 12
        max_sample_value = 2**(sample_width-1)-1
        size             = 2**14-1
        scope_acq0 = struct.unpack('i'*size, struct.pack('i'*size, *[int(max_sample_value/size)*i for i in range(0, size)]))
        scope_acq1 = struct.unpack('i'*size, struct.pack('i'*size, *[max_sample_value-int(max_sample_value/size)*i for i in range(0, size)]))
        for name in acq_out:
            assert acq_out[name]["index"] == acquisitions[name]["index"]
            for sample0, sample1 in zip(scope_acq0, acq_out[name]["acquisition"]["scope"]["path0"]["data"]):
                assert sample0/max_sample_value == sample1
            for sample0, sample1 in zip(scope_acq1, acq_out[name]["acquisition"]["scope"]["path1"]["data"]):
                assert sample0/max_sample_value == sample1
            assert len(acq_out[name]["acquisition"]["bins"]["integration"]["path0"]) == acquisitions[name]["num_bins"]
            assert len(acq_out[name]["acquisition"]["bins"]["integration"]["path1"]) == acquisitions[name]["num_bins"]
            assert len(acq_out[name]["acquisition"]["bins"]["threshold"])            == acquisitions[name]["num_bins"]
            assert len(acq_out[name]["acquisition"]["bins"]["avg_cnt"])              == acquisitions[name]["num_bins"]

        #Clear acquisitions
        wave_and_prog_dict = {"waveforms": {}, "weights": {}, "acquisitions": {}, "program": "stop"}
        with open(os.path.join(tmpdir, "sequence.json"), 'w', encoding='utf-8') as file:
            json.dump(wave_and_prog_dict, file, indent=4)
            file.close()

        #Upload empty acquisition list
        pulsar.sequencer0_waveforms_and_program(os.path.join(tmpdir, "sequence.json"))

        #Check acquisition list content
        acq_out = pulsar.get_acquisitions(0)
        assert acq_out == {}

        #Get acquisition status (dummy returns nonsense)
        assert pulsar.get_acquisition_state(0) == False
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_program_handling(tmpdir):
    """
    Tests program handling function calls.

    Parameters
    ----------
    tmpdir
        Temporary directory

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check program handling function calls
        generic.test_program_handling(pulsar, tmpdir)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_scope_acquisition_control():
    """
    Tests scope acquisition control function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Reading and writing to a single parameter will trigger a read and write of all sequencer settings.
        pulsar.scope_acq_sequencer_select(1)
        assert pulsar.scope_acq_sequencer_select() == 1
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()

#------------------------------------------------------------------------------
def test_sequencer_control():
    """
    Tests program handling function calls.

    Parameters
    ----------

    Returns
    ----------

    Raises
    ----------
    """

    #Instantiate Pulsar
    pulsar = pulsar_qrm_dummy("pulsar")

    try:
        #Check sequencer control function calls
        generic.test_sequencer_control(pulsar)
    except Exception:
        raise
    finally:
        #Close Pulsar
        pulsar.close()
